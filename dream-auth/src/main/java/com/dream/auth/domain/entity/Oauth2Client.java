package com.dream.auth.domain.entity;

import lombok.Data;

@Data
public class Oauth2Client {
    private int id;
    private String clientId;
    private String clientName;
    private String clientSecret;
    private String redirectUrl;
    private String grantType;
    private String authorized_grant_types;
    private String scope;
    private boolean autoapprove;
}
