package com.dream.auth.domain.models;

import lombok.Data;

@Data
public class SsoAuthDto {
    private String client_id;
    private String response_type;
    private String redirect_uri;
}
