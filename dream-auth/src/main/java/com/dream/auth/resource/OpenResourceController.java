package com.dream.auth.resource;

import com.dream.auth.domain.entity.SysUser;
import com.dream.auth.domain.models.LoginUser;
import com.dream.auth.service.SysPermissionService;
import com.dream.auth.web.service.TokenService;
import com.dream.core.core.domain.AjaxResult;
import com.dream.web.utils.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/resource")
public class OpenResourceController {
    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private TokenService tokenService;
    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public AjaxResult getInfo() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }
}
