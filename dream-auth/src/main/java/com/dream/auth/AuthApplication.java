package com.dream.auth;

import com.dream.auth.utils.SecurityUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

import java.time.Duration;
import java.time.Instant;

@SpringBootApplication
@MapperScan("com.dream.auth.mapper")
@ComponentScan(basePackages = {"com.dream"})
@EnableDiscoveryClient
public class AuthApplication {
    private static final Logger log = LoggerFactory.getLogger(AuthApplication.class);
    public static void main(String[] args) {
        Instant inst1 = Instant.now();
        SpringApplication.run(AuthApplication.class, args);
        log.info(":: 启动成功!耗时:{}秒 ::", Duration.between(inst1, Instant.now()).getSeconds());
    }
}
