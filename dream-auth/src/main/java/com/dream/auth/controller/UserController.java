package com.dream.auth.controller;

import com.alibaba.fastjson.JSON;
import com.dream.auth.domain.entity.SysMenu;
import com.dream.auth.domain.entity.SysUser;
import com.dream.auth.domain.models.LoginBody;
import com.dream.auth.domain.models.LoginUser;
import com.dream.auth.service.ISysMenuService;
import com.dream.auth.service.SysLoginService;
import com.dream.auth.service.SysPermissionService;
import com.dream.auth.web.service.TokenService;
import com.dream.core.config.DreamAppConfig;
import com.dream.core.constant.Constants;
import com.dream.core.core.domain.AjaxResult;
import com.dream.web.utils.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RequestMapping("/user")
@RestController
public class UserController {
    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysMenuService menuService;
    @Autowired
    private SysLoginService loginService;
    @Autowired
    DreamAppConfig dreamAppConfig;

    /**
     * 登录方法
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginBody loginBody) {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public AjaxResult getInfo() {
        LoginUser loginUser = null;
        if (dreamAppConfig.getSeparate()) {
            loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        } else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            loginUser = (LoginUser) authentication.getPrincipal();
        }
        SysUser user = null;
        // 角色集合
        Set<String> roles = new HashSet<>();
        // 权限集合
        Set<String> permissions = new HashSet<>();
        if (loginUser != null) {
            user = loginUser.getUser();
            roles = permissionService.getRolePermission(user);
            permissions = permissionService.getMenuPermission(user);
        }
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }

    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @GetMapping("getRouters")
    public AjaxResult getRouters() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 用户信息
        SysUser user = loginUser.getUser();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(user.getUserId());
        return AjaxResult.success(menuService.buildMenus(menus));
    }

    @GetMapping("/getDataScope")
    public AjaxResult getDataScope(int userId) {
        System.out.println("傳入參數："+userId);
        Set<String> dataScope = new HashSet<>();
        dataScope.add("a1");
        dataScope.add("a2");
        AjaxResult ajax = AjaxResult.success();
        ajax.put("dataScope", dataScope);
        return ajax;
    }
    @PostMapping("/addDataScope")
    public AjaxResult addDataScope(@RequestBody Map<String,String> data){
        System.out.println("傳入參數："+ JSON.toJSONString(data));
        AjaxResult ajax = AjaxResult.success();
        ajax.put("dataScope", data);
        return ajax;
    }
}
