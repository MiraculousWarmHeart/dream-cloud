package com.dream.auth.controller;

import com.dream.auth.service.ISysMenuService;
import com.dream.auth.service.SysLoginService;
import com.dream.auth.service.SysPermissionService;
import com.dream.auth.web.service.TokenService;
import com.dream.core.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysMenuService menuService;
    @Autowired
    private SysLoginService loginService;
    @RequestMapping("/home")
    public String home(){
        return "index";
    }

}
