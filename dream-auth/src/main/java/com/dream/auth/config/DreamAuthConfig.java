package com.dream.auth.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@Configuration
@RefreshScope
@Data
public class DreamAuthConfig {
    @Value("${oauth2.client.client-id}")
    private String clientId;
    @Value("${oauth2.client.client-secret}")
    private String clientSecret;
}
