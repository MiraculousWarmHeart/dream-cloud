package com.dream.auth.service;

import com.dream.auth.domain.entity.Oauth2Client;

import java.util.List;

public interface IOauth2Service {
    List<Oauth2Client> getOauth2ClientByClientId(String clientId);
}
