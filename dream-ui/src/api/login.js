import request from "@/utils/request";

// 登录方法
export function login(username, password, code, uuid) {
  const data = {
    username,
    password,
    code,
    uuid
  };
  return request({
    url: "/login",
    method: "post",
    data: data
    //data: initFormData(data)
  });
}
export const initFormData = (data = {}) => {
  let ret = ''
  for (let it in data) {
    ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
  }
  return ret.replace(/&$/, '')
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: "/getInfo",
    method: "get"
  });
}

// 退出方法
export function logout() {
  return request({
    url: "/logout",
    method: "post"
  });
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: "/captchaImage",
    method: "get"
  });
}

export function getAuthorizeCode(data) {
  return request({
    url:
      "/oauth/authorize?client_id=" +
      data.client_id +
      "&response_type=" +
      data.response_type +
      "&redirect_uri=" +
      data.redirect_uri +
      "&scope=" +
      data.scope,
    method: "get"
  });
}
export function get_confirm_access(data) {
  return request({
    url:
      "/oauth/confirm_access?client_id=" +
      data.client_id +
      "&response_type=" +
      data.response_type +
      "&redirect_uri=" +
      data.redirect_uri,
    method: "get"
  });
}
export function getAuthorizeCallBack(callBackUrl) {
  return request({
    url: callBackUrl,
    method: "get"
  });
}
