package com.dream.blog.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/oauth2")
public class Oauth2Controller {
    @GetMapping("/callBack")
    public void callBack(){
        System.out.println(JSON.toJSONString("aa"));

    }
}
