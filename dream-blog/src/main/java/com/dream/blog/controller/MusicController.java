package com.dream.blog.controller;

import com.dream.blog.domain.Music;
import com.dream.blog.domain.dto.MusicQueryItem;
import com.dream.blog.service.MusicService;
import com.dream.core.utils.DateUtils;
import com.dream.core.utils.PageRecord;
import com.dream.core.utils.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/blog")
public class MusicController extends BaseController {
    @Autowired
    private MusicService musicService;

    @PostMapping("/getMusicList")
    @ResponseBody
    public R getMusicList(@RequestBody MusicQueryItem query) {
        PageHelper.startPage(query.getPageIndex(), query.getPageSize(), true);
        List<Music> musicList = musicService.getMusicList(query);
        PageInfo<Music> pageInfo = new PageInfo<Music>(musicList);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        PageRecord<Music> pageRecord = new PageRecord<Music>();
        pageRecord.setRows(musicList);
        pageRecord.setCurrentPage(query.getPageIndex());
        pageRecord.setCurrentPageSize(query.getPageSize());
        pageRecord.setTotalCount(total);
        pageRecord.setTotalPage(pages);
        return R.success().put("data", pageRecord);
    }

    @RequestMapping("/music/getMusicDetail/{musicId}")
    public R getMusicDetail(@PathVariable("musicId") int musicId) {
        Music musicItem = musicService.getById(musicId);
        return R.success().put("data", musicItem);
    }

    @RequestMapping("/music/saveMusic")
    @ResponseBody
    public R saveMusic(@RequestBody Music submitItem) {
        boolean result = false;
        try {
            if (submitItem.getId() == 0) {
                submitItem.setCreateOn(DateUtils.getNowDate());
                submitItem.setUpdateOn(DateUtils.getNowDate());
                ;
                result = musicService.saveMusic(submitItem);
            } else {
                submitItem.setUpdateOn(DateUtils.getNowDate());
                result = musicService.updateMusic(submitItem);
            }
        } catch (Exception e) {
            log.error("操作失败:{0}", e);
            log.error(e.getMessage());
        }
        if (result) {
            return R.success("操作成功");
        }
        return R.error("操作失败");
    }

    @RequestMapping("/music/batchSaveMusic")
    @ResponseBody
    public R batchSaveMusic(@RequestBody List<Music> submitItem) {
        boolean result = false;
        try {
            for (Music music : submitItem) {
                music.setCreateOn(DateUtils.getNowDate());
                music.setUpdateOn(DateUtils.getNowDate());
                music.setSortCode(0);
                result = musicService.saveMusic(music);
            }
        } catch (Exception e) {
            log.error("操作失败:{0}", e);
            log.error(e.getMessage());
            result = false;
        }
        if (result) {
            return R.success("操作成功");
        }
        return R.error("操作失败");
    }

    @RequestMapping("/music/deleteMusic/{musicId}")
    @ResponseBody
    public R deleteMusic(@PathVariable int musicId) {
        boolean result = false;
        try {
            result = musicService.deleteMusic(musicId);
        } catch (Exception e) {
            log.error("操作失败:{0}", e);
            log.error(e.getMessage());
        }
        if (result) {
            return R.success("操作成功");
        }
        return R.error("操作失败");
    }

}