package com.dream.blog.service;

import com.dream.blog.domain.ArticleDetail;
import com.dream.blog.domain.ArticleFile;
import com.dream.blog.domain.dto.ArticleItem;
import com.dream.blog.domain.dto.ArticleQueryItem;
import com.dream.blog.domain.dto.ArticleSubmitItem;

import java.util.List;
import java.util.Map;

public interface ArticleService {

    List<ArticleItem> getArticleList(ArticleQueryItem queryItem);

    List<ArticleItem> getRecommendArticleList();

    List<ArticleItem> getTopReadArticleList();

    List<Map<String, Object>> getRelvantArticle(Integer articleId);

    List<Integer> getNoAbstractArticleId();

    ArticleItem getById(int articleId);

    ArticleDetail getContentById(int articleId);

    int saveArticle(ArticleSubmitItem submitItem);

    void saveContent(ArticleDetail submitItem);

    void saveImage(List<ArticleFile> fileList);

    boolean updateArticle(ArticleSubmitItem submitItem);

    void updateAbstractById(int articleId, String abstractContent);

    boolean deleteArticle(int articleId);

    boolean deleteImage(int articleId);

    boolean deleteContent(int articleId);

    void updateArticleViewCount(int articleId, int viewCount);

    void updateArticleTags(int articleId, String articleTags);
}
