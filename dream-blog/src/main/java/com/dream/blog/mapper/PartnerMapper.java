package com.dream.blog.mapper;


import com.dream.blog.domain.Partner;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PartnerMapper {
    List<Partner> getAll();

    int savePartner(Partner submitItem);

    boolean updatePartner(Partner submitItem);

    boolean deletePartner(@Param("partnerId") int partnerId);
}
