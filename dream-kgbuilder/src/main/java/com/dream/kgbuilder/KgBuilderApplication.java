package com.dream.kgbuilder;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@MapperScan("com.dream.kgbuilder.mapper")
@EnableDiscoveryClient
@EnableFeignClients("com.dream.kgbuilder.openfeign")
public class KgBuilderApplication {
    public static void main(String[] args) {
        SpringApplication.run(KgBuilderApplication.class, args);
    }
}
