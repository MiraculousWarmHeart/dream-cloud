package com.dream.kgbuilder.service;


import java.util.List;
import java.util.Map;

public interface KnowledgeGraphService {
    List<Map<String, Object>> getDomains();

    List<Map<String, Object>> getDomainList(String domainName, String createUser);

    void saveDomain(Map<String, Object> map);

    void updateDomain(Map<String, Object> map);

    void deleteDomain(Integer id);

    List<Map<String, Object>> getDomainByName(String domainName);

    List<Map<String, Object>> getDomainById(Integer domainId);

    void saveNodeImage(List<Map<String, Object>> mapList);

    void saveNodeContent(Map<String, Object> map);

    void updateNodeContent(Map<String, Object> map);

    List<Map<String, Object>> getNodeImageList(Integer domainId, Integer nodeId);

    List<Map<String, Object>> getNodeContent(Integer domainId, Integer nodeId);

    void deleteNodeImage(Integer domainId, Integer nodeId);
}
