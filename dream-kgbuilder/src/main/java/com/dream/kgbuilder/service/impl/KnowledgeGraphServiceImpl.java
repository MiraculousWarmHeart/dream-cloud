package com.dream.kgbuilder.service.impl;

import com.dream.kgbuilder.mapper.KnowledgeGraphMapper;
import com.dream.kgbuilder.service.KnowledgeGraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class KnowledgeGraphServiceImpl implements KnowledgeGraphService {

    @Autowired
    private KnowledgeGraphMapper knowledgeGraphRepository;

    @Override
    public List<Map<String, Object>> getDomainList(String domainName, String createUser) {
        return knowledgeGraphRepository.getDomainList(domainName, createUser);
    }

    @Override
    public void saveDomain(Map<String, Object> map) {
        knowledgeGraphRepository.saveDomain(map);
    }

    @Override
    public void updateDomain(Map<String, Object> map) {
        knowledgeGraphRepository.updateDomain(map);
    }

    @Override
    public void deleteDomain(Integer id) {
        knowledgeGraphRepository.deleteDomain(id);
    }

    @Override
    public List<Map<String, Object>> getDomainByName(String domainName) {
        return knowledgeGraphRepository.getDomainByName(domainName);
    }

    @Override
    public List<Map<String, Object>> getDomains() {
        return knowledgeGraphRepository.getDomains();
    }

    @Override
    public List<Map<String, Object>> getDomainById(Integer domainId) {
        return knowledgeGraphRepository.getDomainById(domainId);
    }

    @Override
    public void saveNodeImage(List<Map<String, Object>> mapList) {
        knowledgeGraphRepository.saveNodeImage(mapList);
    }

    @Override
    public void saveNodeContent(Map<String, Object> map) {
        knowledgeGraphRepository.saveNodeContent(map);
    }

    @Override
    public void updateNodeContent(Map<String, Object> map) {
        knowledgeGraphRepository.updateNodeContent(map);
    }

    @Override
    public List<Map<String, Object>> getNodeImageList(Integer domainId, Integer nodeId) {
        return knowledgeGraphRepository.getNodeImageList(domainId, nodeId);
    }

    @Override
    public List<Map<String, Object>> getNodeContent(Integer domainId, Integer nodeId) {
        return knowledgeGraphRepository.getNodeContent(domainId, nodeId);
    }

    @Override
    public void deleteNodeImage(Integer domainId, Integer nodeId) {
        knowledgeGraphRepository.deleteNodeImage(domainId, nodeId);
    }

}
