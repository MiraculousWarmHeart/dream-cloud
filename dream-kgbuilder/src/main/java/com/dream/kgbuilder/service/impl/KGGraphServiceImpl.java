package com.dream.kgbuilder.service.impl;

import com.dream.kgbuilder.dao.KGraphMapper;
import com.dream.kgbuilder.domain.dto.GraphNode;
import com.dream.kgbuilder.domain.dto.GraphPageRecord;
import com.dream.kgbuilder.domain.dto.GraphQuery;
import com.dream.kgbuilder.service.KGGraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
//@Primary
public class KGGraphServiceImpl implements KGGraphService {

    @Autowired
    //@Qualifier("KGraphRepository")
    private KGraphMapper kgRepository;

    @Override
    public GraphPageRecord<HashMap<String, Object>> getPageDomain(GraphQuery queryItem) {
        return kgRepository.getPageDomain(queryItem);
    }

    @Override
    public void deleteKGDomain(String domain) {
        kgRepository.deleteKGDomain(domain);
    }

    @Override
    public HashMap<String, Object> getDomainGraph(GraphQuery query) {
        return kgRepository.getDomainGraph(query);
    }

    @Override
    public HashMap<String, Object> getDomainNodes(String domain, Integer pageIndex, Integer pageSize) {
        return kgRepository.getDomainNodes(domain, pageIndex, pageSize);
    }

    @Override
    public long getRelationNodeCount(String domain, long nodeId) {
        return kgRepository.getRelationNodeCount(domain, nodeId);
    }

    @Override
    public void createDomain(String domain) {
        kgRepository.createDomain(domain);
    }

    @Override
    public HashMap<String, Object> getMoreRelationNode(String domain, String nodeId) {
        return kgRepository.getMoreRelationNode(domain, nodeId);
    }

    @Override
    public HashMap<String, Object> updateNodeName(String domain, String nodeId, String nodeName) {
        return kgRepository.updateNodeName(domain, nodeId, nodeName);
    }

    @Override
    public HashMap<String, Object> createNode(String domain, GraphNode entity) {
        return kgRepository.createNode(domain, entity);
    }

    @Override
    public HashMap<String, Object> batchCreateNode(String domain, String sourceName, String relation,
                                                   String[] targetNames) {
        return kgRepository.batchCreateNode(domain, sourceName, relation, targetNames);
    }

    @Override
    public HashMap<String, Object> batchCreateChildNode(String domain, String sourceId, Integer entitytype,
                                                        String[] targetNames, String relation) {
        return kgRepository.batchCreateChildNode(domain, sourceId, entitytype, targetNames, relation);
    }

    @Override
    public List<HashMap<String, Object>> batchCreateSameNode(String domain, Integer entityType, String[] sourceNames) {
        return kgRepository.batchCreateSameNode(domain, entityType, sourceNames);
    }

    @Override
    public HashMap<String, Object> createLink(String domain, long sourceId, long targetId, String ship) {
        return kgRepository.createLink(domain, sourceId, targetId, ship);
    }

    @Override
    public HashMap<String, Object> updateLink(String domain, long shipId, String shipName) {
        return kgRepository.updateLink(domain, shipId, shipName);
    }

    @Override
    public List<HashMap<String, Object>> deleteNode(String domain, long nodeId) {
        return kgRepository.deleteNode(domain, nodeId);
    }

    @Override
    public void deleteLink(String domain, long shipId) {
        kgRepository.deleteLink(domain, shipId);
    }

    @Override
    public HashMap<String, Object> createGraphByText(String domain, Integer entitytype, Integer operatetype,
                                                     Integer sourceId, String[] rss) {
        return kgRepository.createGraphByText(domain, entitytype, operatetype, sourceId, rss);
    }

    @Override
    public void batchCreateGraph(String domain, List<Map<String, Object>> params) {
        kgRepository.batchCreateGraph(domain, params);
    }

    @Override
    public void updateNodeFileStatus(String domain, long nodeId, int status) {
        kgRepository.updateNodeFileStatus(domain, nodeId, status);
    }

    @Override
    public void updateCorrdOfNode(String domain, String uuId, Double fx, Double fy) {
        kgRepository.updateCorrdOfNode(domain, uuId, fx, fy);
    }

    @Override
    public void batchInsertByCSV(String domain, String csvUrl, int status) {
        kgRepository.batchInsertByCSV(domain, csvUrl, status);
    }
}
