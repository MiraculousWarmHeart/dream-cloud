package com.dream.kgbuilder.openfeign;

import com.dream.core.config.ServiceFeignConfig;
import com.dream.core.core.domain.AjaxResult;
import com.dream.core.config.FeignBasicAuthConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@Component
public class AuthUserFeignService {
    /**
     *      /user/getDataScope?userId=111
     *
     *
     *   1)、让所有请求过网关；
     *          1、@FeignClient("dream-gateway")：dream-gateway所在的机器发请求
     *          2、/user/getDataScope?userId=111
     *   2）、直接让后台指定服务处理
     *          1、@FeignClient("dream-gateway")
     *          2、/user/getDataScope?userId=111
     *
     * @return
     */
    @GetMapping("/auth/user/getDataScope")
    public AjaxResult getDataScope(@RequestParam("userId")int userId) {

        return AjaxResult.error("userId失败");
    }

    @PostMapping("/auth/user/addDataScope")
    public AjaxResult addDataScope(@RequestBody Map<String,String> data) {
        return AjaxResult.error("data失败");
    }
}
