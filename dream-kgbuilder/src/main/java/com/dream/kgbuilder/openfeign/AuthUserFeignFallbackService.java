package com.dream.kgbuilder.openfeign;

import com.dream.core.config.FeignBasicAuthConfiguration;
import com.dream.core.config.ServiceFeignConfig;
import com.dream.core.core.domain.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(value = "dream-auth",configuration = {FeignBasicAuthConfiguration.class, ServiceFeignConfig.class},fallback = AuthUserFeignService.class)
public interface AuthUserFeignFallbackService {
    /**
     *      /user/getDataScope?userId=111
     *
     *
     *   1)、让所有请求过网关；
     *          1、@FeignClient("dream-gateway")：dream-gateway所在的机器发请求
     *          2、/user/getDataScope?userId=111
     *   2）、直接让后台指定服务处理
     *          1、@FeignClient("dream-gateway")
     *          2、/user/getDataScope?userId=111
     *
     * @return
     */
    @GetMapping("/auth/user/getDataScope")
    public AjaxResult getDataScope(@RequestParam("userId")int userId);

    @PostMapping("/auth/user/addDataScope")
    public AjaxResult addDataScope(@RequestBody Map<String,String> data);
}
