package com.dream.kgbuilder.domain.dto;

import lombok.Data;

@Data
public class GraphQuery {

    private int domainId;
    private String domain;
    private String nodeName;
    private String[] relation;
    private int matchType;
    private int pageSize = 10;
    private int pageIndex = 1;

}
