package com.dream.kgbuilder.domain.dto;

import lombok.Data;

@Data
public class GraphNode {
    private long uuid;
    private String name;//显示名称
    private String color;//对应关系数据库字段
    private Integer r;
}
