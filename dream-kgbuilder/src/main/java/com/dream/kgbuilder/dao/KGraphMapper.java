package com.dream.kgbuilder.dao;

import com.dream.kgbuilder.domain.dto.GraphNode;
import com.dream.kgbuilder.domain.dto.GraphPageRecord;
import com.dream.kgbuilder.domain.dto.GraphQuery;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface KGraphMapper {
    /**
     * 领域标签分页
     */
    GraphPageRecord<HashMap<String, Object>> getPageDomain(GraphQuery queryItem);

    /**
     * 删除Neo4j 标签
     */
    void deleteKGDomain(String domain);

    /**
     * 查询图谱节点和关系
     */
    HashMap<String, Object> getDomainGraph(GraphQuery query);

    /**
     * 获取节点列表
     */
    HashMap<String, Object> getDomainNodes(String domain, Integer pageIndex, Integer pageSize);

    /**
     * 获取某个领域指定节点拥有的上下级的节点数
     */
    long getRelationNodeCount(String domain, long NodeId);

    /**
     * 创建领域,默认创建一个新的节点,给节点附上默认属性
     */
    void createDomain(String domain);

    /**
     * 获取/展开更多节点,找到和该节点有关系的节点
     */
    HashMap<String, Object> getMoreRelationNode(String domain, String NodeId);

    /**
     * 更新节点名称
     */
    HashMap<String, Object> updateNodeName(String domain, String NodeId, String NodeName);

    /**
     * 创建单个节点
     */
    HashMap<String, Object> createNode(String domain, GraphNode entity);

    /**
     * 批量创建节点和关系
     */
    HashMap<String, Object> batchCreateNode(String domain, String sourceName, String relation, String[] targetNames);

    /**
     * 批量创建下级节点
     */
    HashMap<String, Object> batchCreateChildNode(String domain, String sourceId, Integer entityType,
                                                 String[] targetNames, String relation);

    /**
     * 批量创建同级节点
     */
    List<HashMap<String, Object>> batchCreateSameNode(String domain, Integer entityType, String[] sourceNames);

    /**
     * 添加关系
     */
    HashMap<String, Object> createLink(String domain, long sourceId, long targetId, String ship);

    /**
     * 更新关系
     */
    HashMap<String, Object> updateLink(String domain, long shipId, String shipName);

    /**
     * 删除节点(先删除关系再删除节点)
     */
    List<HashMap<String, Object>> deleteNode(String domain, long NodeId);

    /**
     * 删除关系
     */
    void deleteLink(String domain, long shipId);

    /**
     * 段落识别出的三元组生成图谱
     */
    HashMap<String, Object> createGraphByText(String domain, Integer entityType, Integer operateType, Integer sourceId,
                                              String[] rss);

    /**
     * 批量创建节点，关系
     */
    void batchCreateGraph(String domain, List<Map<String, Object>> params);

    /**
     * 更新节点有无附件
     */
    void updateNodeFileStatus(String domain, long NodeId, int status);

    /**
     * 导入csv
     */
    void batchInsertByCSV(String domain, String csvUrl, int status);

    void updateCorrdOfNode(String domain, String uuId, Double fx, Double fy);
}
