package com.dream.kgbuilder.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
public interface KnowledgeGraphMapper {
    List<Map<String, Object>> getDomains();

    List<Map<String, Object>> getDomainList(@Param("domainName") String domainName, @Param("createUser") String createUser);

    List<Map<String, Object>> getRelationshipList(@Param("domainId") Integer domainId, @Param("relationType") Integer relationType, @Param("shipname") String shipname);

    void saveDomain(@Param("params") Map<String, Object> map);

    void updateDomain(@Param("params") Map<String, Object> map);

    void deleteDomain(@Param("id") Integer id);

    List<Map<String, Object>> getDomainByName(@Param("domainName") String domainName);

    List<Map<String, Object>> getDomainById(@Param("domainId") Integer domainId);

    void saveNodeImage(@Param("mapList") List<Map<String, Object>> mapList);

    void saveNodeContent(@Param("params") Map<String, Object> map);

    void updateNodeContent(@Param("params") Map<String, Object> map);

    List<Map<String, Object>> getNodeImageList(@Param("domainId") Integer domainId, @Param("nodeId") Integer nodeid);

    List<Map<String, Object>> getNodeContent(@Param("domainId") Integer domainId, @Param("nodeId") Integer nodeid);

    void deleteNodeImage(@Param("domainId") Integer domainId, @Param("nodeId") Integer nodeId);
}
