package com.dream.gateway.config;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
public class NacosConfig {
    /**
     * 用于改变程序自动获取的本机ip
     */
    //@Bean
    //@Primary
  /*  public NacosDiscoveryProperties nacosProperties() throws UnknownHostException {
        NacosDiscoveryProperties nacosDiscoveryProperties = new NacosDiscoveryProperties();
        InetAddress localHost = InetAddress.getLocalHost();
        //域名
        String hostName = localHost.getHostName();
        //ip
        String hostAddress = localHost.getHostAddress();
        System.out.println("*****" + localHost +"," + hostName + ", " + hostAddress);
        nacosDiscoveryProperties.setIp(hostName);
        return nacosDiscoveryProperties;
    }*/
    /**
     * 用于改变程序自动获取的本机ip
     */
    @Bean
    @Primary
    public NacosDiscoveryProperties nacosProperties() {
        NacosDiscoveryProperties nacosDiscoveryProperties = new NacosDiscoveryProperties();
        nacosDiscoveryProperties.setIp("localhost");
        return nacosDiscoveryProperties;
    }
}
