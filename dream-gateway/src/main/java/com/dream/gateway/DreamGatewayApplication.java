package com.dream.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class,SecurityAutoConfiguration.class},scanBasePackages = {"com.dream"})
@EnableDiscoveryClient
//@ComponentScan(basePackages = {"com.dream.gateway","com.dream.common"})
public class DreamGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(DreamGatewayApplication.class, args);
    }

}
