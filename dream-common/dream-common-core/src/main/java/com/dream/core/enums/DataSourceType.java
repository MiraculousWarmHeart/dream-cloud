package com.dream.core.enums;

/**
 * 数据源
 *
 * @author dream
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
