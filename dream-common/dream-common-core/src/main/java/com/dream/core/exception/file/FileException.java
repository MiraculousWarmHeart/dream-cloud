package com.dream.core.exception.file;

import com.dream.core.exception.BaseException;

/**
 * 文件信息异常类
 *
 * @author dream
 */
public class FileException extends BaseException {
    private static final long serialVersionUID = 1L;

    public FileException(String code, Object[] args) {
        super("file", code, args, null);
    }

}
